// const nightwatchCucumber = require('nightwatch-cucumber');

require('nightwatch-cucumber')({
  cucumberArgs: [
    '--require', 'test/e2e/step_definitions', 
    '--format', 'json:tmp/reports/cucumber.json',
    // '--tags', '@login',
    //'--format', 'node_modules/cucumber-pretty',
    // '--tag', process.env.TAG || '',
    'test/e2e/features',
  ]
})

module.exports = {
  "src_folders" : 'test/e2e/features',
  "output_folder" : "tmp/reports",
  "custom_commands_path" : "",
  "custom_assertions_path" : "",
  "page_objects_path" : "test/e2e/page-objects",
  "globals_path" : "",

  "selenium" : {
    "start_process" : false,
    "server_path" : "",
    "log_path" : "reports",
    "port" : 4444,
    "cli_args" : {
      "webdriver.chrome.driver" : "",
      "webdriver.gecko.driver" : "",
      "webdriver.edge.driver" : ""
    }
  },

  "test_settings" : {
    "default" : {
      "launch_url" : 'https://' + ('www.linkedin.com/'),
      "selenium_port"  : 4444,
<<<<<<< HEAD
      "selenium_host"  : '192.168.2.33',
=======
      "selenium_host"  : (process.env.SELENIUM_HOST || 'localhost'),
>>>>>>> 9070d29fd493d8464cc6319f974997e84510d348
      "silent": true,
      "screenshots" : {
        "enabled" : true,
        "on_failure" : true,
        "on_error" : false,
        "path" : "tmp/screenshots"
      },
      "desiredCapabilities": {
        "browserName": "chrome",
        "chromeOptions" : {
          "args" : ["start-fullscreen"]
        }
      }
    },

    "firefox": {
      "desiredCapabilities": {
        "browserName": "firefox",
        "marionette": true,
        "javascriptEnabled": true,
        "acceptSslCerts": true
      }
    },

    "test-server" : {
      "selenium" : {
        "start_process" : false
      },
      "selenium_host" : ('192.168.2.33'),
      "selenium_port" : 4444,
      "silent" : true,
    },

    "saucelabs" : {
      "selenium" : {
        "start_process" : false
      },
      "selenium_host" : "ondemand.saucelabs.com",
      "selenium_port" : 80,
      "silent" : true,
      "username" : process.env.SAUCE_USERNAME,
      "access_key" : process.env.SAUCE_ACCESS_KEY,
      "use_ssl" : false,
      "output" : true,
      "desiredCapabilities": {
        "name" : "Thingspine-tests",
        "browserName": "chrome"
      }
    },

    "browserstack" : {
      "selenium" : {
        "start_process" : false
      },
      "selenium_host" : "hub.browserstack.com",
      "selenium_port" : 80,
      "silent" : true,
      "desiredCapabilities": {
        "name" : "Thingspine-tests",
        "browserName": "chrome",
        "javascriptEnabled": true,
        "browserstack.user": process.env.BROWSERSTACK_USERNAME,
        "browserstack.key": process.env.BROWSERSTACK_KEY
      }
    }

  }
}
