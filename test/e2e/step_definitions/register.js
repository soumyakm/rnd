const { client } = require('nightwatch-cucumber');

const { Given, Then, When } = require('cucumber');

const register = client.page.register();

Given(/^I am on the home page$/, () => {
    return register.isBodyDisplayed();
});

Then(/^the home page exists$/,() => { 
    return register.isDisplayedWithDelay('@registerform');
});
Then(/^the page contains sign up form with title"(.*?)"$/,(title) => {
    return register.isDisplayedWithDelay('@mainheading');
}); 
Then(/^contains a subtitle"(.*?)"$/,(title) => {
    return register.isDisplayedWithDelay('@subheading');
});
