#features/register.feature

 @register @Task1

Feature: User Registration

  As a User
  I want to register to LinkedIn
  In order to access different features

# Scenario: Display home page header elements 

#   Given I am on the home page
#   And the home page exists   
#   And the page header contains logo on top left
#   And contains "Email or phone number" textbox 
#   And contains "Password" textbox 
#   And contains "Sign in" button
#   And contains "Forgot password?" link

Scenario: Display Sign Up form 

  Given I am on the home page
  And the home page exists   
  And the page contains sign up form with title "Be great at what you do"
  And contains a subtitle "Get started - it's free."

# Scenario: Sign Up form elements

#   Given I am on the sign up form at home page
#   And the form contains textboxes with labels "First name", "Last name", "Email or phone number" and "Password" with info "(6 or more characters)"
#   And the form conatins terms and services text content as "By clicking Join now, you agree to the LinkedIn User Agreement, Privacy Policy, and Cookie Policy."
#   And the form contains "Join now" button  

# Scenario: Unsuccessful register with all fields blank

#   Given I am on the sign up form at home page
#   When I give no values for "First name", "Last name", "Email or phone number" and "Password" with info "(6 or more characters)" fields
#   And I click on "Join now" button
#   Then I should see the message "Please enter your first name"


# Scenario: Unsuccessful register with invalid Email id

#   Given I am on the sign up form at home page
#   And I give invalid value for "Email or phone number" field
#   And I click on "Join now" button
#   Then I should see the message "Please enter a valid email address or mobile number"

#   Examples :
#   |First name          |Last name             |Email or phone number        |Password(6 or more characters)   
